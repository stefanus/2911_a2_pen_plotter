import java.util.Comparator;
import java.util.HashMap;

public class StateComparator implements Comparator<DrawState> {

	/**
	 * Overriding the comparator by getting the value of g that is stored in the DrawState. 
	 */
	@Override
	public int compare(DrawState e1, DrawState e2) {

		if (e1.getFValue() < e2.getFValue()) {
			return -1;
		} else if (e1.getFValue() > e2.getFValue()) {
			return 1;
		}

		return 0;

	}

}