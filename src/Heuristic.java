
public interface Heuristic {

	public double getHValue(DrawState current);
}
