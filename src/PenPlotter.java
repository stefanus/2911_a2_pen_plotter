import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class PenPlotter {

	/**
	 * The main program that will take in and process the input from the argument. 
	 * @param args The argument will take in a file input.
	 */
	public static void main(String[] args) {
		
		// Create a new searcher object that will be use to search and get the states.
		Searcher search = new Searcher();

		// INPUT FROM A FILE AND READING FROM IT TESTER.
		try {Scanner s = new Scanner(new FileReader(args[0])); // args[0] is the
																// first command
																// line argument

			// VARIABLE INITIALISATION.
			int totalLine = 0;
			ArrayList<Point2D> points = new ArrayList<Point2D>();
			ArrayList<Line> lineToDraw = new ArrayList<Line>();
			
			// LOOP THROUGH THE ENTIRE FILE INPUT.
			while (s.hasNext() && s.hasNextLine()) {

				String[] input = s.nextLine().split(" ");

				// SETTING FOR THE POINTS THAT ARE NEEDED IN EACH LINE. P1 AND P2.
				int x1 = Integer.parseInt(input[2]);
				int y1 = Integer.parseInt(input[3]);

				int x2 = Integer.parseInt(input[5]);
				int y2 = Integer.parseInt(input[6]);

				Point2D point1 = new Point2D.Double();
				Point2D point2 = new Point2D.Double();

				point1.setLocation(x1, y1);
				point2.setLocation(x2, y2);

				// SET THE LINE THAT ARE NEEDED TO BE DRAWN.
				Line newLine = new Line(point1, point2);

				lineToDraw.add(newLine);
				
				// PASS IN A SET OF UNIQUE POINTS IN THE ENTIRE DRAWING.
				if (!points.contains(point1)) {
					points.add(point1);
				}
				if (!points.contains(point2)) {
					points.add(point2);
				}

				totalLine++;
			}

			search.pathSearching(points, totalLine, lineToDraw);

			s.close();

		} catch (FileNotFoundException e) {
		}
	}

}
