import java.awt.HeadlessException;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

public class Searcher {


	/**
	 * This method will start the searching of the path. 
	 * @param points This will store all the unique points that are in the lines that needed to be drawn. 
	 * @param totalLine This will store the total number of line that is needed to be drawn for checking purposes.
	 * @param lineToDraw This will store all the lines that are needed to be drawn in an array list. 
	 * @return It will return a path that to the goal. 
	 */
	public List<Point2D> pathSearching(ArrayList<Point2D> points,
			int totalLine, ArrayList<Line> lineToDraw) {

		// VARIABLES INITIALISATION. 
		LinkedList<Point2D> path = new LinkedList<Point2D>();

		int nodesExpanded = 0;

		PriorityQueue<DrawState> que = new PriorityQueue<DrawState>(50,
				new StateComparator());
		ArrayList<DrawState> stateVisited = new ArrayList<DrawState>();
		// END OF VARIABLES INITIALISATION.

		// GENERATE FIRST LEVEL STATE WHERE IT START AND ADD IT TO THE QUE. 
		Point2D startingPen = new Point2D.Double();
		startingPen.setLocation(0, 0);
		ArrayList<Line> lineDrawnSoFar = new ArrayList<Line>();
		DrawState headState = new DrawState(startingPen, startingPen, null,
				lineDrawnSoFar, lineToDraw);
		
		que.add(headState);
		// END OF GENERATING THE START OF THE STATE.
		
		DrawState parent = null;

		// START THE PRIORITY QUE AND POPPING OFF AND ADD TO THE QUE
		while (!que.isEmpty()) {

			parent = que.remove();

			nodesExpanded++;

			stateVisited.add(parent);

			if (parent.numLineDrawnSoFar() == totalLine) {

				double linesSum = 0;
				// TOTAL COST FOR THE LINES. 
				for (Line lines : lineToDraw) {
					linesSum += lines.getFrom().distance(lines.getTo());

				}
				// END OF TOTAL COST FOR THE LINES. 
				
				// START OF PRINTIING FOR THE RESULT. 
				double result =  (parent.getGValue() + linesSum);
				
				System.out.println(nodesExpanded + " nodes expanded" );
				System.out.format("cost = %.2f%n" , result);
				Point2D pen = new Point2D.Double(0, 0);
				for (Line l : parent.getLineDrawnArray()) {

					int penX = (int) pen.getX();
					int penY = (int) pen.getY();
					int lGetFromX = (int) l.getFrom().getX();
					int lGetFromY = (int) l.getFrom().getY();
					int lGetToX = (int) l.getTo().getX();
					int lGetToY = (int) l.getTo().getY();
					if (pen.equals(l.getFrom())) {

					} else {
						System.out.println("Move from " + penX + " "
								+ penY + " to " + lGetFromX
								+ " " + lGetFromY);
					}
					System.out.println("Draw from " + lGetFromX + " "
							+ lGetFromY + " to " + lGetToX
							+ " " + lGetToY );
					pen = l.getTo();
				}
				
				// END OF THE PRINTING FOR THE RESULT. 
				break;

			}

			// CREATE NEXT STATE AND LOOP THROUGH IT WHEN THE FINAL STATE IS NOT REACH YET. 

			ArrayList<DrawState> neighbours = whatToDraw(parent);

			for (DrawState s : neighbours) {
				// ADDING HEURISTIC FOR LATER ON. TO DO

				 ImplementStrategy aStrategy = new ImplementStrategy();
				// THIS IS THE PART WHERE THAT CALL FOR SETTING THE HEURISTIC AND AS OF NOW IS ZERO.
				 s.setHValue(aStrategy.getHValue(s));

				que.add(s);

			}

		}

		return path;
	}

	/**
	 * This method will generate the next set of states from the current state that are given. 
	 * @param current This will be the given state from the input.
	 * @return It will return an ArrayList of DrawState from the given current state.
	 */
	public ArrayList<DrawState> whatToDraw(DrawState current) {

		// INITIALISING VARIABLES.
		ArrayList<DrawState> whatToDraw = new ArrayList<DrawState>();
		ArrayList<Line> lineLeft = new ArrayList<Line>();
		lineLeft.addAll(current.getLineLeftArray());

		// LOOP THROUGH VIA THE NUMBER LINES LEFT FOR THE PARTICULAR STATE. 
		for (int i = 0; i < lineLeft.size(); i++) {

			Point2D pointFrom = lineLeft.get(i).getFrom();
			Point2D pointTo = lineLeft.get(i).getTo();

			// TO GET THE LINE DRAWN SO FAR FOR THIS PARTICULAR STATE. 
			ArrayList<Line> lineDrawnSoFar = new ArrayList<Line>();
			lineDrawnSoFar.addAll(current.getLineDrawnArray());
			lineDrawnSoFar.add(lineLeft.get(i));

			//CREATE TEMP OF ARRAY TO STORE THE LINE LEFT TO BE PUT INTO THE NEXT STATE.
			ArrayList<Line> temp = new ArrayList<Line>();
			temp.addAll(lineLeft);
			temp.remove(lineLeft.get(i));

			// CREATE THE NEW STATE FOR THE LINE THAT IS DRAWN FROM
			DrawState stateFromPoint = new DrawState(pointFrom, pointTo,
					current, lineDrawnSoFar, temp);

			Line invert = new Line(lineLeft.get(i).getTo(), lineLeft.get(i)
					.getFrom());
			lineDrawnSoFar = new ArrayList<Line>();
			lineDrawnSoFar.addAll(current.getLineDrawnArray());
			lineDrawnSoFar.add(invert);

			// CREATE THE NEW STATE FOR THE LINE THAT IS DRAWN TO
			DrawState stateToPoint = new DrawState(pointTo, pointFrom, current,
					lineDrawnSoFar, temp);
			

			// ADD THE LINE TO THE ARRAY LIST THAT WILL BE RETURNED. 
			whatToDraw.add(stateFromPoint);
			whatToDraw.add(stateToPoint);

		}

		return whatToDraw;
	}


}
