import java.awt.geom.Point2D;


public class Line {
	
	private Point2D from;
	private Point2D to;
	
	/**
	 * This method will construct the object line that contain 2 Point2D one is from point and to point. 
	 * @param from this will store the start of the line drawn.
	 * @param to this will store the end of the line drawn.
	 */
	public Line(Point2D from, Point2D to){
		this.from = from;
		this.to = to;
	}
	
	/**
	 * This method will return the from point for this particular line
	 * @return This will return a Point2D for this particular line. 
	 */
	public Point2D getFrom(){
		return from;
	}
	
	/**
	 * This method will return the To point for this particular line
	 * @return This will return a Point2D for this particular line.
	 */
	public Point2D getTo(){
		return to;
	}
	
	/**
	 * This method will return the distance between the 2 points. 
	 * @return This will return a double 
	 */
	public double getDistance(){

		
		return from.distance(to);
	}

}
