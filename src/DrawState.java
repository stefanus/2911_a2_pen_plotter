import java.awt.geom.Point2D;
import java.util.ArrayList;

public class DrawState {

	private double		gValue;
	private double		hValue;
	private double		fValue;
	private Point2D		pen;
	private DrawState	parent;

	ArrayList<Line>		lineDrawnSoFarArray	= new ArrayList<Line>();
	ArrayList<Line>		lineLeftArray		= new ArrayList<Line>();

	/**
	 * This is the state constructor, it will take the paremeter below to construct a new state. 
	 * @param pen - this will store the current pen location as a Point2D
	 * @param pen2 - this will store the end of the pen location drawing at the end of the line.
	 * @param parent - this will store the parent state on this particular state. 
	 * @param lineDrawnSoFarArray - this will store an ArrayList<Line> drawn so far for this particular state
	 * @param lineLeftArray - this will store an ArrayList<Line> of what is the line that is left to be drawn for this particular state. 
	 * 
	 */

	public DrawState(Point2D pen,Point2D pen2, DrawState parent,
			ArrayList<Line> lineDrawnSoFarArray, ArrayList<Line> lineLeftArray) {
		this.parent = parent;
		this.pen = pen2;
		this.fValue = 0;
		this.hValue = 0;
		this.lineLeftArray = lineLeftArray;
		
		if (parent == null) {
			this.gValue = 0;
			
		} else {
			this.gValue = parent.getGValue() + parent.getPen().distance(pen);
			this.lineDrawnSoFarArray= lineDrawnSoFarArray;
		}

	}

	/**
	 * This method will set the H value for the particular state
	 * @param heuristicValue It will pass in a heuristic value and set it to the current state hValue. 
	 */
	public void setHValue(double heuristicValue){
		this.hValue = heuristicValue;
	}
	
	/**
	 * This method will get the hValue for this particular DrawState
	 * @return It will return a double for the hValue for this particular DrawState
	 */
	public double getHValue(){
		return this.hValue;
	}
	
	/**
	 * This method will return an ArrayList<Line> for the line left in the array. 
	 * @return it will return an ArrayList<Line> from this particular state. 
	 */
	public ArrayList<Line> getLineLeftArray() {
		return lineLeftArray;
	}

	/**
	 * This method will return an ArrayList<Line> for the line that has been drawn in an array.  
	 * @return it will return an ArrayList<Line> from this particular state.
	 */
	public ArrayList<Line> getLineDrawnArray() {
		return lineDrawnSoFarArray;
	}

	/**
	 * This method will return a DrawState of the parent of the current DrawState that is passed in. 
	 * @return It will return a DrawState
	 */
	public DrawState getParent() {
		return parent;
	}

	/**
	 * This method will return the pen location for this particular state. 
	 * @return It will return a Point2D object for the pen. 
	 */
	public Point2D getPen() {
		return pen;
	}
	
	


	/**
	 * This method will return the count of number of line left to be drawn for this particular state. 
	 * @return It will return an integer of the count number of the line left in this particular state. 
	 */
	public int numLineLeftToDraw() {
		int lineLeftToDraw = 0;

		lineLeftToDraw = lineLeftArray.size();
		return lineLeftToDraw;
	}

	/**
	 * This method will return the count of number of line that has been drawn so far for this particular state.
	 * @return It will return an integer of the count number of the line drawn so far in this particular state.
	 */
	public int numLineDrawnSoFar() {
		int lineDrawnSoFar = 0;

		if (lineDrawnSoFarArray == null) {
			lineDrawnSoFar = 0;
		} else {
			lineDrawnSoFar = lineDrawnSoFarArray.size();
		}

		return lineDrawnSoFar;
	}

	/**
	 * This method will return the gValue for this particular DrawState
	 * @return It will return a double for the gValue
	 */
	public double getGValue() {

		return gValue;
	}

	/**
	 * This method will return the fValue for this particular DrawState
	 * @return It will return a double for the fValue
	 */
	public double getFValue() {

		double fValue = 0;
		fValue = this.gValue + this.hValue;
		return fValue;
	}
}
